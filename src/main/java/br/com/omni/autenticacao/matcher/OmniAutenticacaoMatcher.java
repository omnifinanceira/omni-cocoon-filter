package br.com.omni.autenticacao.matcher;

import org.apache.avalon.framework.parameters.Parameters;
import org.apache.cocoon.environment.ObjectModelHelper;
import org.apache.cocoon.environment.Request;
import org.apache.cocoon.matching.WildcardURIMatcher;
import org.apache.cocoon.sitemap.PatternException;

import java.util.Map;

public class OmniAutenticacaoMatcher extends WildcardURIMatcher {


    @Override
    @SuppressWarnings(("java:S3740"))
    public Map match(String pattern, Map objectModel, Parameters parameters) throws PatternException {
        Map map = super.match(pattern, objectModel, parameters);

        Request request = ObjectModelHelper.getRequest(objectModel);

        if(map != null) {
            String parameter = request.getParameter(parameters.getParameter("omniKey", "p_omni_key"));
            map.put("omniKey", parameter);
        }
        return map; // parameter defined, return map
    }
}
