package br.com.omni.autenticacao.config;

import br.com.omni.autenticacao.exception.ConfigException;
import br.com.omni.autenticacao.gateway.filter.OmniAutenticacaoFilter;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {

	private static Properties properties;

	private static final String PROPERTY_FILE = "config.properties";

	private static final String JNDI_DATASOURCE_PROPERTY = "jndi.dataSource";

	private static final String INSERT_FORBIDEN_LOG_PROPERTY = "insertForbidenLog";

	private Config() {
	}

	static {
		loadPropertyFile();
	}

	private static void loadPropertyFile() {
		InputStream input = OmniAutenticacaoFilter.class.getClassLoader().getResourceAsStream(PROPERTY_FILE);
		properties = new Properties();
		try {
			if (input == null) {
				throw new ConfigException("Erro ao ler o arquivo " + PROPERTY_FILE);
			}
			properties.load(input);
		} catch (IOException e) {
			throw new ConfigException(e.getMessage(), e);
		}
	}

	public static String jndiDataSource() {
		return properties.getProperty(JNDI_DATASOURCE_PROPERTY);
	}

	public static boolean insertForbidenLogProperty() {
		return Boolean.parseBoolean(properties.getProperty(INSERT_FORBIDEN_LOG_PROPERTY, "false"));
	}
}
