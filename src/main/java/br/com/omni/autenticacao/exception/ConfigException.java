package br.com.omni.autenticacao.exception;

public class ConfigException extends RuntimeException {
	
	private static final long serialVersionUID = 7070411477343490872L;
	
	public ConfigException(String message) {
		super(message);
	}
	
	public ConfigException(String message, Throwable cause) {
		super(message, cause);
	}
}
