package br.com.omni.autenticacao.gateway.cache;

import java.util.Optional;

public class MockCacheLogin implements CacheLogin {
    @Override
    public Optional<String> get(String key) {
        return Optional.empty();
    }

    @Override
    public void put(String key, String value) {
        // Utilizado como mock.
    }
}
