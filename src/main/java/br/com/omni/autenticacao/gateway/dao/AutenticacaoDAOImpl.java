package br.com.omni.autenticacao.gateway.dao;

import br.com.omni.autenticacao.exception.DAOException;
import br.com.omni.autenticacao.gateway.jdbc.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AutenticacaoDAOImpl implements AutenticacaoDAO {

    private Logger log = LoggerFactory.getLogger(AutenticacaoDAOImpl.class);

    public AutenticacaoDAOImpl(){
        log.debug("Instanciando AutenticacaoDAOImpl");
    }

    public String findKey(String key) throws DAOException {
        String query = "SELECT KEY FROM TAB_AUTENTICACAO WHERE KEY = ? " +
                "AND (DT_EXPIRACAO IS NULL OR DT_EXPIRACAO > SYSDATE)";

        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement pst = connection.prepareStatement(query)) {


            pst.setString(1, key);
            try (ResultSet rs = pst.executeQuery()){
                String strKey = null;
                if(rs.next()) {
                    strKey = rs.getString("KEY");
                    log.debug("findByKey retornou {}", strKey);
                }
                return strKey;
            }
        } catch(SQLException | NamingException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    public String findUrl(String url) throws DAOException {
        String query = "SELECT URL FROM COCOONFILTER.URL_RELATORIO WHERE LOWER(?) LIKE LOWER(URL || '%')";

        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement pst = connection.prepareStatement(query)) {

            pst.setString(1, url);
            try (ResultSet rs = pst.executeQuery()){
                String strUrl = null;
                if(rs.next()) {
                    strUrl = rs.getString("URL");
                    log.debug("findUrl retornou {}", strUrl);
                }
                return strUrl;
            }
        } catch(SQLException | NamingException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    public int insertForbidenLog(HttpServletRequest request) throws DAOException {
        String query = "INSERT INTO FORBIDEN_LOG(ID, PROCEDURE, DATA, LOG) VALUES(SYS_GUID(), ?, SYSTIMESTAMP, ?)";

        try (Connection connection = ConnectionFactory.getConnection();
             PreparedStatement pst = connection.prepareStatement(query)) {

            pst.setString(1, request.getRequestURI());
            String refererUrl = request.getHeader("referer");
            String userAgent = request.getHeader("User-Agent");

            StringBuffer strLog = new StringBuffer();
            strLog.append("query_string: ").append(request.getQueryString()).append(" \n ").append("http_cookie: ");
            if(request.getCookies() != null) {
                for (Cookie c : request.getCookies()) {
                    strLog.append(c.getName()).append("=").append(c.getValue());
                }
            }
            strLog.append(" \n ").append("request_url: ").append(request.getRequestURL()).append(" \n ")
                .append("http_referer: ").append(refererUrl).append(" \n ")
                .append("http_user_agent: ").append(userAgent).append(" \n ")
                .append("request_method: ").append(request.getMethod());

            pst.setString(2, strLog.toString());

            return pst.executeUpdate();
        } catch(SQLException | NamingException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }
}
