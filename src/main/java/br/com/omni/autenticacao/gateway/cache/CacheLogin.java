package br.com.omni.autenticacao.gateway.cache;

import java.util.Optional;

public interface CacheLogin {

	Optional<String> get(String key);

	void put(String key, String value);

}
