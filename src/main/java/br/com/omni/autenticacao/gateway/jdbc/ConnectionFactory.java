package br.com.omni.autenticacao.gateway.jdbc;

import br.com.omni.autenticacao.config.Config;
import br.com.omni.autenticacao.exception.ConnectionException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionFactory {

    private static InitialContext initialContext;

    static {
        try {
            initialContext  = new InitialContext();
        } catch (NamingException e) {
            throw new ConnectionException("Erro ao inicializar o service locator", e);
        }
    }

    private ConnectionFactory(){}

    public static DataSource getDataSource() throws NamingException {
        return (DataSource) initialContext.lookup(Config.jndiDataSource());
    }

    public static Connection getConnection() throws NamingException, SQLException {
       DataSource ds = getDataSource();
       return  ds.getConnection();
    }
}
