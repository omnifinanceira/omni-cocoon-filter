package br.com.omni.autenticacao.gateway.cache;

import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class EhCacheLogin implements CacheLogin {

	private Logger log = LoggerFactory.getLogger(EhCacheLogin.class);
	private static final String CACHE_ALIAS = "autenticacao";
	private CacheManager cacheManager;
	private Cache<String, String> autenticacaoCache;

	public EhCacheLogin() {
		if(cacheManager == null) {
			log.debug("Instanciando cacheManager");
			cacheManager = CacheManagerBuilder
					.newCacheManagerBuilder().build();
			cacheManager.init();
	
			autenticacaoCache = cacheManager
					.createCache(CACHE_ALIAS, CacheConfigurationBuilder
							.newCacheConfigurationBuilder(String.class, String.class, ResourcePoolsBuilder.heap(10))
							.withExpiry(Expirations.timeToLiveExpiration(Duration.of(10, TimeUnit.MINUTES))));
			log.debug("cacheManager instanciado");
		}
	}

	@Override
	public Optional<String> get(String key) {
		log.debug("get {}", key);
        if(key == null) {
            throw new NullPointerException("Key is required!");
        }

        String value = autenticacaoCache.get(key);

        if(value == null || value.equals("")) {
            value = null;
			log.debug("get {} retornou vazio", key);
        }

        return Optional.ofNullable(value);
    }

    @Override
    public void put(String key, String value) {
		log.debug("put {}, {}", key, value);
		autenticacaoCache.put(key, value);
	}
}
