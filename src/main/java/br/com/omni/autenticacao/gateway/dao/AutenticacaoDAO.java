package br.com.omni.autenticacao.gateway.dao;

import br.com.omni.autenticacao.exception.DAOException;

import javax.servlet.http.HttpServletRequest;

public interface AutenticacaoDAO {

    String findKey(String key) throws DAOException;
    String findUrl(String url) throws DAOException;
    int insertForbidenLog(HttpServletRequest request) throws DAOException;
}
