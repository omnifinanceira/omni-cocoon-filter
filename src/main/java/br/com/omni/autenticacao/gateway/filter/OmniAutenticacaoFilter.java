package br.com.omni.autenticacao.gateway.filter;

import br.com.omni.autenticacao.config.Config;
import br.com.omni.autenticacao.exception.DAOException;
import br.com.omni.autenticacao.gateway.cache.CacheLogin;
import br.com.omni.autenticacao.gateway.cache.EhCacheLogin;
import br.com.omni.autenticacao.gateway.dao.AutenticacaoDAO;
import br.com.omni.autenticacao.gateway.dao.AutenticacaoDAOImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")
public class OmniAutenticacaoFilter implements Filter {

	private Logger log = LoggerFactory.getLogger(OmniAutenticacaoFilter.class);

	private CacheLogin cacheLogin;

	private AutenticacaoDAO autenticacaoDAO;

	private static final String PARAM_OMNI_KEY = "p_omni_key";

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		cacheLogin = new EhCacheLogin();
		autenticacaoDAO = new AutenticacaoDAOImpl();
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;

		String omniKey = getOmniKey(httpServletRequest);

		try {
			String requestURI = httpServletRequest.getRequestURI();
			if ("/pdf/health".equals(requestURI)) {
				log.trace("Acesso Permitido {}", requestURI);
				chain.doFilter(request, response);
			} else if (validarAcesso(requestURI, omniKey)) {
				log.info("Acesso Permitido {} com p_omni_key={}", requestURI, omniKey);
				chain.doFilter(request, response);
			} else {
				log.info("Acesso Negado {} com p_omni_key={}", requestURI, omniKey);
				if(Config.insertForbidenLogProperty()) {
					autenticacaoDAO.insertForbidenLog(httpServletRequest);
				}
				httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Acesso Negado");
			}
		} catch (DAOException e) {
			log.error(e.getMessage(), e);
			throw new ServletException("Erro ao validar omni-key");
		}
	}

	private String getOmniKey(HttpServletRequest httpServletRequest) {
		return httpServletRequest.getParameter(PARAM_OMNI_KEY);
	}

	private boolean validarAcesso(String uri, String key) throws DAOException {
		return !validarUri(uri) || (parametroValido(key) && keyValida(key));
	}

	private boolean validarUri(String uri) throws DAOException {
		String resultUri = autenticacaoDAO.findUrl(uri);
		boolean validar = resultUri != null;
		log.info("uri {} {}", uri, validar ? "validar" : "não validar" );
		return validar;
	}

	private boolean parametroValido(String key) {
		return !isEmpty(key);
	}

	private boolean keyValida(String key) throws DAOException {
		boolean keyValida = false;
		if (cacheLogin.get(key).isPresent()) {
			log.info("key {} recuperado via cache", key);
			keyValida = true;
		} else {
			log.info("validando key no banco de dados");
			String keyExistente = autenticacaoDAO.findKey(key);
			if(keyExistente != null) {
				cacheLogin.put(key, keyExistente);
				keyValida = true;
			}
		}
		return keyValida;
	}

	private boolean isEmpty(String param) {
		return param == null || param.isEmpty();
	}

	@Override
	public void destroy() {
		// Obrigatório
	}
}
