package br.com.omni.autenticacao.gateway.dao;

import br.com.omni.autenticacao.exception.DAOException;

import javax.servlet.http.HttpServletRequest;

public class AutenticacaoMockDAO implements AutenticacaoDAO {
    @Override
    public String findKey(String key) throws DAOException {
        return "fsdfjdfjs";
    }

    @Override
    public String findUrl(String url) throws DAOException {
        return "validar";
    }

    @Override
    public int insertForbidenLog(HttpServletRequest request) throws DAOException {
        return 1;
    }
}
