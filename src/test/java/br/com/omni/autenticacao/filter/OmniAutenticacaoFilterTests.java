package br.com.omni.autenticacao.filter;

import br.com.omni.autenticacao.gateway.cache.CacheLogin;
import br.com.omni.autenticacao.gateway.dao.AutenticacaoDAOImpl;
import br.com.omni.autenticacao.gateway.filter.OmniAutenticacaoFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OmniAutenticacaoFilterTests {
    @Mock
    private CacheLogin cacheLogin;

    @Mock
    private AutenticacaoDAOImpl autenticacaoDAO;

    @InjectMocks
    private OmniAutenticacaoFilter omniAutenticacaoFilter;


    @Test
    public void acessoPermitido_naoValidarUri() throws Exception {
        String uri = "/relatorio/teste?p_omni_key=12321321" ;

        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain chain = mock(FilterChain.class);

        when(request.getRequestURI()).thenReturn(uri);
        when(autenticacaoDAO.findUrl(uri)).thenReturn(null);

        omniAutenticacaoFilter.doFilter(request, response, chain);

        verify(chain, atLeastOnce()).doFilter(request, response);
    }

    @Test
    public void acessoNaoPermitido_parametroInvalido() throws Exception {
        String uri = "/relatorio/teste?p_omni_key=" ;
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain chain = mock(FilterChain.class);

        when(request.getRequestURI()).thenReturn(uri);

        when(autenticacaoDAO.findUrl(uri)).thenReturn(uri);

        omniAutenticacaoFilter.doFilter(request, response, chain);

        verify(chain, never()).doFilter(request, response);
        verify(response, atLeastOnce()).sendError(anyInt(), anyString());
    }

    @Test
    public void acessoPermitido_keyExistenteCache() throws Exception {
        String key = "123456ABC";
        String uri = "/relatorio/teste?p_omni_key=" + key;
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain chain = mock(FilterChain.class);

        when(request.getParameter(anyString())).thenReturn(key);
        when(request.getRequestURI()).thenReturn(uri);

        when(autenticacaoDAO.findUrl(uri)).thenReturn(uri);

        when(cacheLogin.get(key)).thenReturn(Optional.of(key));

        omniAutenticacaoFilter.doFilter(request, response, chain);

        verify(chain, atLeastOnce()).doFilter(request, response);
    }

    @Test
    public void acessoPermitido_keyExistenteBancoDados() throws Exception {
        String key = "123456ABC";
        String uri = "/relatorio/teste?p_omni_key=" + key;
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain chain = mock(FilterChain.class);

        when(request.getParameter(anyString())).thenReturn(key);
        when(request.getRequestURI()).thenReturn(uri);

        when(autenticacaoDAO.findUrl(uri)).thenReturn(uri);
        when(cacheLogin.get(key)).thenReturn(Optional.empty());
        when(autenticacaoDAO.findKey(key)).thenReturn(key);

        omniAutenticacaoFilter.doFilter(request, response, chain);

        verify(chain, atLeastOnce()).doFilter(request, response);
    }

    @Test
    public void acessoNaoPermitido_keyNaoExistenteBancoDadosCache() throws Exception {
        String key = "123456ABC";
        String uri = "/relatorio/teste?p_omni_key=" + key;
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        FilterChain chain = mock(FilterChain.class);

        when(request.getParameter(anyString())).thenReturn(key);
        when(request.getRequestURI()).thenReturn(uri);

        when(autenticacaoDAO.findUrl(uri)).thenReturn(uri);
        when(cacheLogin.get(key)).thenReturn(Optional.empty());
        when(autenticacaoDAO.findKey(key)).thenReturn(null);

        omniAutenticacaoFilter.doFilter(request, response, chain);

        verify(chain, never()).doFilter(request, response);
        verify(response, atLeastOnce()).sendError(anyInt(), anyString());
    }
}
